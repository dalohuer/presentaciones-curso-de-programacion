#! /usr/bin/awk -f

#ID Continent,Continent,ID Country,Country,ID Year,Year,ID HS0,HS0,ID HS2,HS2,FOB US

BEGIN{
FS=",";
fields["continente"]=2;
fields["pais"]=4;
fields["hso"]=8
fields["hs2"]=10;
len=length(fields);
}{
count=1;
# iteración sobre una lista de elementos de un array o map
for (field in fields){
column=fields[field]
printf $column;
if(count<len){printf ";"}
else if(count==len){
printf "\n"
}
count+=1;
}
}
