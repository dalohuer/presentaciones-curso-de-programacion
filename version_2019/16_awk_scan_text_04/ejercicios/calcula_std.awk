@include "estadistica.awk"

(NR>1){
# acumular la temperatura
temp=$3;
temperatura[$1]=temp;
}
END{
# calcular promedio y std
promedio=calc_promedio(temperatura);
std = calc_std(temperatura, promedio);
print "La temperatura promedio fue: "promedio" y la desviación estandar: "std;
}
