#+TITLE: Lenguaje para el escaneo y procesamiento de AWK
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:1 num:f
#+OPTIONS: ^:nil
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+BEAMER_THEME: PaloAlto
#+BEAMER_HEADER: \setbeamertemplate{navigation symbols}{}
#+COLUMNS: %5ITEM %10BEAMER_ENV(Env) %5BEAMER_ACT(Act) %6BEAMER_COL(Col)
#+latex_class_options: [10pt]

* Temario de la clase

** ¿Qué veremos en esta clase?

- Lectura de archivos y streams :: Se explicará el modo en cómo se construye un
     *comando* o *programa* con ~awk~, leyendo desde un archivo o desde el ~stream~.

- Identificación de separadores :: Una característica clave para poder seccionar
     la información

- Selección de campos :: Se aprenderá a realizar una lectura de archivos de
     textos (o streams), identificación de separadores y selección de campos. 

- Uso y operación de strings :: Se aplicará el uso de ~strings~ dentro de la operación de
     lectura de archivos y se realizarán operaciones con estos.

- Las variables de estado :: Se identificará el uso de las variables de estado
     que cambian durante la lectura de un archivo con ~awk~. FS y OFS.

* Registrar inicio de clases

** Una marca en la línea de comandos: con nombre de clase

Para poder realizar una búsqueda que tenga un mayor contexto y precisión, 
es posible añadir más texto a la seña que nos permita mejorar la referencia.

Recomiendo añadir, al número de la clase, la seña relacionada al nombre de la
clase, que en este caso sería *awk_01*.

#+BEGIN_SRC bash
echo "CLASE_13::awk_01"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_13::awk_01*

* Lectura de archivos y streams con awk

** EL lenguaje ~awk~

Este lenguaje les permitirá trabajar con detalle información basada en *texto*.
Veremos, a lo largo de /cinco/ sesiones, los detalles más importantes que les
entrega esta herramienta.

El lenguaje Awk es el complemento perfecto a las herramientas que provee bash, ya que 
permite procesar texto línea a línea en base a algún carácter separador. El nombre AWK deriva de las iniciales de 
los apellidos de sus autores: Alfred Aho, Peter Weinberger, y Brian Kernighan según la [[https://es.wikipedia.org/wiki/AWK][Wikipedia]].

Permite definir variables, trabajar con listas, estructuras de control, expresiones regulares, etc. Todo
lo que se necesita en un lenguaje de programación y es la pieza que faltaba para poder sacarle todo el jugo
a la terminal.

** El manual de ~awk~

Para una primera mirada de *Awk* haz:

#+BEGIN_SRC bash
man awk
#+END_SRC

O bien, desde [[http://gen.lib.rus.ec][Libgen]] puedes descargar

- sed & awk, Arnold Robbins
- The awk manual
- Effective awk programming, Arnold Robbins

** Lectura de archivos

La lectura de archivos con ~awk~ se realiza de la siguiente forma, permitiendo
uno o más archivos en la misma instancia de ejecución.

#+BEGIN_EXAMPLE
awk [OPCIONES] 
    'BEGIN{#inicio}{#operacion}END{#final}' ARCHIVO(s) 
#+END_EXAMPLE
 
La manera más simple sería

#+BEGIN_EXAMPLE
awk '{#operacion}' ARCHIVO 
#+END_EXAMPLE

** Lectura de streams

La lectura de archivos con ~awk~ se realiza de la siguiente forma, permitiendo
uno o más archivos en la misma instancia de ejecución.

#+BEGIN_EXAMPLE
comando | awk [OPCIONES] 
          'BEGIN{#inicio}{#operacion}END{#final}' 
#+END_EXAMPLE

La manera más simple sería

#+BEGIN_EXAMPLE
comando | awk '{#operacion}'  
#+END_EXAMPLE

** Lectura desde una entrada de texto EOF

Otra forma que se puede utilizar es mediante el ingreso de un texto mediante la
acción *EOF*

#+BEGIN_EXAMPLE
awk 'BEGIN{#inicio}{#operación}END{#fin}' << EOF
texto_linea_1
texto_linea_2
...
...
texto_linea_n
EOF
#+END_EXAMPLE

** De manera general

De esta forma, la acción que lleva a cabo ~awk~ se puede entender de la manera
siguiente:

[[file:./img/entendiendo_awk.png]]

* Selección de campos

** ¿Qué son los campos?

Cuando trabajamos con una línea de texto, o una cadena de texto, podemos
identificar como *campos* aquellas /subcadenas/ que están separadas por
espacios. 

Ocupamos la función *print* para imprimir 

Desde la misma terminal de bash haremos lo siguiente:


#+BEGIN_SRC bash
frase="un tigre, dos tigres, tres tigres"
# también funciona esto, cargando string con "<<<"
# awk '{print $0}' <<< $frase
echo $frase| awk '{print $0}'
echo $frase| awk '{print $1}'
echo $frase| awk '{print $2}'
echo $frase| awk '{print $3}'
#+END_SRC

** Algunos extras de bash

+ var=$(<nombre_archivo) :: carga el contenido de archivo a variable
+ comando $<<$ EOF :: permite ingresar un documento línea a línea y terminar con EOF
+ comando $<<<$  string :: carga un string que se desea utilizar 

*** Referencias

+ Uso de $<$,$<<$,$<<<$ :: https://askubuntu.com/questions/678915/whats-the-difference-between-and-in-bash

** De manera visual, podemos tener

Cada campo o palabra tiene asignado una posición que va desde 
*1 hasta la cantidad total de campos*, de manera general, podemos ver o
imaginarnos lo siguiente:

file:./img/campos.png

** ¿Cómo escoger un campo específico de cada línea?

Para escoger los campos específicos, que están numerados de manera secuencial,
debemos conocer

- ¿Cúales son los campos?
- ¿Qué número tiene cada campo?

Luego, para escoger el campo con el número *N*, se utiliza la expresión *$N*
para obtener el valor de N.

Por ejemplo, si deseo mostrar el campo *N=4*.

#+BEGIN_SRC bash
echo $frase| awk '{print $4}'
#+END_SRC

** ¿Cómo mostrar un conjunto de campos?

De manera análoga, para mostrar varios campos a la vez, debemos poner el símbolo
separador *,* para ingresar cada campo de manera separada.

#+BEGIN_SRC bash
# mostrar los campos pares
echo $frase| awk '{print $2, $4, $6}'
#+END_SRC

* Identificación de string separador

** El " " como separador por defecto

Una vez que ya sabemos cómo construir y operar con ~awk~, necesitamos
profundizar en el reconocimiento del carácter separador o bien, cuando lo
necesitemos, la cadena de caracteres que nos permiten separar o seccionar la
información. 

Por defecto, el string separador de ~awk~ corresponde al espacio *" "*

#+BEGIN_SRC bash
echo $frase| awk '{print $1, $2, $3, $4, $5, $6}'
#+END_SRC

** Cambio en el separador de campos

Sin embargo, la /flexibilidad/ de este lenguaje es que podemos escoger cualquier
carácter o cadena de caracteres. La opción *-F ', '* permite separar por *, *
(coma y espacio) la línea. 

#+BEGIN_SRC bash
echo $frase| awk -F', ' '{print $1, $2, $3}'
#+END_SRC

** A manera de resumen

La misma frase o texto puede ser manipulada en base al separador de campos, este
determina con claridad con cuantos campos cuenta ~awk~ para procesar ese trozo
de información.

[[file:./img/cambio_campos.png]]

* Uso y operación de strings
** Concatenar o juntar strings

   Es una de las operaciones más sencillas, ya que basta con poner juntos un campo con el otro, o bien una
variable con otra.

#+BEGIN_SRC bash
echo $frase |awk '{print "Esto es concatenar: "$1$2}' 
echo $frase |awk -F', ' '{
             print "Esto es concatenar (con separador 
             \", \"): "$1$2}' 
#+END_SRC

** Obtener el largo

   Se utiliza la función *length*, que entrega el largo o tamaño del string.

#+BEGIN_SRC bash
echo $frase |awk '{print "Largo del campo 2:"length($2)}'
echo $frase |awk -F', ' '{print "Largo del campo 2 
                          (separador ", "):"length($2)}'
#+END_SRC

** Cortar una sección

Una operación muy común en cualquier lenguaje es *recortar* un string.

Se utiliza la función *substr(s, i [, n])* de la siguiente manera: 

- Se selecciona la posición del string y la cantidad de carácteres a extraer. 
- La posición i comienza desde el 1. 
- Si no existe la parte *[,n]* la función considera la extracción desde i hasta el final.

#+BEGIN_SRC bash
# en toda la fila, mostrar desde el caracter 5 en adelante
echo $frase |awk '{print substr($0, 5)}' 
# en toda la fila, cortar desde el caracter 5, dos caracteres
echo $frase |awk '{print substr($0, 5, 2)}' 
# en segundo campo, con separador ", " recortar desde el caracter 3
echo $frase |awk -F', ' '{print substr($2, 3)}' 
#+END_SRC

** Transformar tamaños de letras

Se utilizan las funciones *toupper* para pasar a mayúsculas y *tolower* para pasar
a minúsculas. Veamos los ejemplos.

#+BEGIN_SRC bash
# pasar todo el texto a mayúsculas
echo $frase |awk '{print toupper($0)}'
# pasar solo el segundo campo a mayúsculas, con separador ', '
echo $frase |awk -F', ' '{print toupper($2)}'
# paser a minúsculas todo 
echo "HOLA Mundo" |awk '{print tolower($0)}'
#+END_SRC


** Un ejercicio con awk y bash

Escribir un *script o programa* que solicite el nombre y el apellido.

- En caso de estar mal escritos lo corrija
- Descartar signos no alfabéticos
- Que muestre el primer caracter en mayúsculas y el resto en minúsculas

Crear un codigo que capitalice el string que se ingrese.

Ocupando:

- substr
- length
- concatenación

Ejemplo:

#+BEGIN_SRC bash
echo "hola mundo!"|awk '{
primera_letra=substr($0,1,1); 
resto_frase=substr($0,2,length($0));
print toupper(primera_letra)resto_frase}'
#+END_SRC

** Buscar coincidencias y obtener el índice o posición

Se utiliza la funcion *index(var, find)* y retorna la posición en que se encuentra esa /cadena a buscar/.

#+BEGIN_SRC bash
echo $frase |awk '{print index($0,"tigre")}'
#+END_SRC

** Buscar y reemplazar

Esta operación permite buscar algún patrón, utilizando regex, y reemplazarlo por 
alguna otra cadena de texto.

De la misma manera que con ~sed~, podemos buscar y reemplazar coincidencias,
ahora con una mayor precisión ya que podemos acceder a los campos en particular.

[[file:./img/sed_vs_awk.png]]

** Reemplazo al detalle

La función *gensub(r, s, h [, t])* permite realizar la búsqueda de la manera siguiente:

- texto a buscar (r) :: un string o regex 
- texto a reemplazar (s) :: lo que cambiará
- condición de reemplazo (h) :: {g,G,n>0}, 
- sobre el que se hace la búsqueda (t) :: si no aparece entonces toma como /*t*:$0/.

Para los valores de h se tiene:

- g o G :: hace un reemplazo en todas las ocurrencias
- valor numérico n>0 :: reemplaza la ocurrencia de la posición n por el string definido.

** Esquema de reemplazo general (h=g)

Para reemplazar *tigre->gato*, la operación funciona así:

[[file:./img/buscar_reemplazar.png]]


** Reemplazando por posición

Realizar el reemplazo para la coincidencia general, primera, segunda y tercera.

#+BEGIN_SRC bash
echo $frase | awk '{print gensub("tigre","gato","g")}'
echo $frase | awk '{print gensub("tigre","gato","1")}'
echo $frase | awk '{print gensub("tigre","gato","2")}'
echo $frase | awk '{print gensub("tigre","gato","3")}'
#+END_SRC

** Separar un string y obtener una lista (array)

La función *split(s,var,separador)* permite separar una entrada de 
string en base a un conjunto de carácteres que definimos como *separador*

¿Qué pide esta función?

1. El string que deseas separar
2. El nombre de la variable o arreglo
3. El caracter separador

Luego, al ejecutarse, carga las partes del string separado en el arreglo.

** ¿Cómo opera la función split?

Teniendo un texto, se escoge separador, al operar carga en una *lista o array*
los valores de cada campo separado.

[[file:./img/split_separar_a_lista.png]]

** En la frase siguiente usar separador "|"

#+BEGIN_SRC bash
lista="la|ta|ra mo|no|wo|po"
echo $lista|awk '{split($1,arreglo,"|"); 
                  print length(arreglo),
                  arreglo[1],arreglo[2], arreglo[3]}'
echo $lista|awk '{split($2,arreglo,"|"); 
                  print length(arreglo),
                  arreglo[1], arreglo[2],
                  arreglo[3], arreglo[4]}'
#+END_SRC

Permite manipular campos que contengan carácteres o conjuntos de carácteres
que puedan utilizarse como separadores.

El conteo de los campos del array comienza desde 1, al estilo *awk*.

** Imprimir carácteres con formato

Una función que permite manipular con mayor detalle la impresión en terminal de
valores es *printf* (con la *f*) ya que nos permitirá definir los formatos con que cada valor
se visualizará. 

La estructura de la acción que realiza la función *printf* es:

#+BEGIN_SRC awk
peso=72.5;
printf "Nombre: %-5s, peso: %3.4f kg", "David", peso;  
#+END_SRC


** ¿Qué son los formatos?

    Son expresiones que le permiten decir a la función *printf* el tipo
de información y cómo la debe imprimir.

+ %s  :: string, cadena de caracteres                           
+ %d  :: entero, número entero                                  
+ %Nd :: entero con N cifras, número entero con cantidad de cifras definidas
+ %f  :: real (float) con cifras por defecto, número real                                    
+ %N.Mf :: real (float) con N cifras enteras y M decimales, número real con cantidades definidas           

**** Referencia a modificadores de formatos

- Guía :: https://www.gnu.org/software/gawk/manual/html_node/Format-Modifiers.html#Format-Modifiers

** Imprimir en formatos

Para imprimir los valores que correspondan de una manera adecuada a veces 
es bueno decirle a la función *printf* que tipo de valores y cuantos carácteres
usa este valor.

Estamos investigando una receta para un pastel para 100 personas, tenemos el problema de que los
valores de volumen están en ~galón~ y los valores de peso en ~libra~, debemos
pasarlos a ~litros~ y ~kilogramos~. Además, no nos dan la cantidad de *huevos*,
sino un volumen de líquido de *huevos*.


** Receta de pastel para 100 personas

La tarea es ordenar los datos en un *csv* en tres columnas, una para el nombre
del elemento, otra para el volumen y otra para el peso. Luego el valor debe ser
puesto en la columna correspondiente. Finalmente, calcular la receta en litros y
kilogramos, usando ~awk~ y la función *printf*.

#+BEGIN_EXAMPLE
Pastel para 100 personas
=======================
azúcar 6 lib.
mantequilla 2 lib.
leche 1 gal.
huevo 1.5 gal. 
harina 12 lib.
escencia de vainilla 0.3 gal. 
ralladura de limón 1.4 lib.
#+END_EXAMPLE

** Añadir la receta a la selección literaria

Crea una nueva sección relacionada con recetas de comida. Agrega la receta del
pastel para 100 personas. Actualiza tu proyecto ~git~.

* Cerrando la clase

** Fin de clase

¡Bienvenido/a al mundo de la programación!

~Guarda tus comandos :)~

