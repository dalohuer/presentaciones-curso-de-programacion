#+TITLE: Conocimientos iniciales de un sistema controlador de versiones: git básico, I
#+AUTHOR: Raquel Bracho, David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:t num:t
#+OPTIONS: ^:nil
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+BEAMER_THEME: PaloAlto
#+COLUMNS: %45ITEM %10BEAMER_ENV(Env) %10BEAMER_ACT(Act) %4BEAMER_COL(Col)
#+latex_class_options: [10pt]
#+latex_header: \usepackage{setspace}
#+latex_header: \onehalfspacing


* Temario de Clases

** ¿Qué veremos en esta clase?

- Aprender que es controlar versiones de un proyecto ::
Cómo gestionar un proyecto que está contenido en un directorio, que comprende
archivos y carpetas.

- Aprender de literatura ::
De la selección global de todos los compañeros y compañeras hablaremos sobre
nuestros gustos literarios y textos escogidos.

- Aprender git ::
Es una herramienta de software que, mediante un método relativamente sencillo,
permite controlar versiones de proyectos incluso cuando hay muchas personas
participando de él.

* Registrar Inicio de Clases
** Una marca en la línea de comandos

Todas las clases deberemos registrar el inicio de clases para resguardar y
tener almacenadas nuestras acciones.

Usaremos el comando ~echo~ para dejar una marca

#+BEGIN_SRC bash
echo "CLASE_03"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_03*, también podrías anotar el
número de la lista en ~history~.

* El sistema para controlar versiones git

** ¿Qué es git?

#+CAPTION: Mr Robot explica ~git~
#+NAME:   fig: hackerman
#+ATTR_HTML: width="150px"
#+ATTR_ORG: :width 150
#+attr_latex: :width 150px
[[file:./img/git_hackerman.png]]


** ¿Qué es “control de versiones”?


Cuando *gestionamos* (editamos, subimos, eliminamos) cambios sobre los elementos 
de algún proyecto o configuración específica. 

Es como actualizar código al añadir los cambios que creemos convenientes 
y luego subirlos a una plataforma de repositorios.


#+CAPTION: Control de versiones
#+NAME:   fig: control_versiones_simple
#+ATTR_HTML: width="200px"
#+ATTR_ORG: :width 200
#+attr_latex: :width 200px
[[file:./img/control_versiones_simple.png]]


** Git es como una bitácora de trabajo

Es así, si tenemos un proyecto con el siguiente archivo, que la escritora
/Gabriela Mistral/ está escribiendo *Amor amor*.

#+BEGIN_EXAMPLE
Anda libre en el surco, bate el ala en el viento,
late vivo en el sol y se prende al pinar.
No te vale olvidarlo como al mal pensamiento:
¡le tendrás que escuchar!
#+END_EXAMPLE

Desde nuestra herramienta, podemos decir que el proyecto /Poema *Amor amor*/
está en una primera versión.

Entonces, la escritora decide registrar en su cuaderno de notas o /bitácora/, lo
que hizo. Ella puso.

- Comentario 1 :: Ya escritos los primeros versos de *Amor amor*, creo que puedo
                  encontrar más palabras a esto que me quema las entrañas.


** TODO Git trabaja sobre las diferencias entre un paso y otro

Luego de un rato, escribe otros cuatro versos:

#+BEGIN_EXAMPLE
Habla lengua de bronce y habla lengua de ave,
ruegos tímidos, imperativos de mar.
No te vale ponerle gesto audaz, ceño grave:
Grazna profundo en su nido
#+END_EXAMPLE

Y la escritora, talentosa ella, escribe como segundo comentario.

- Comentario 2 :: Siento que este poema está logrando expresar sonantemente los
                  sentimientos que han nacido desde mi corazón. ¿Qué dirá el
                  ave? 

Entonces, nuestra talentosa poeta ha dejado registro de su trabajo, al que
podemos acceder a su evolución-

Aquí el sistema ~git~ detectaría que se han añadido /4 nuevas líneas/ al archivo.

** Git detecta cambio en líneas que ya existen: correcciones literarias.

Nuestra escritora reflexiona sobre el texto, considera que el último verso
escrito no calza con lo que busca, por lo que decide hacer un cambio.

#+BEGIN_EXAMPLE
Habla lengua de bronce y habla lengua de ave,
ruegos tímidos, imperativos de mar.
No te vale ponerle gesto audaz, ceño grave:
¡lo tendrás que hospedar!
#+END_EXAMPLE

¡Ahora parece que si está funcionando bien el texto!

** Creamos un nuevo comentario para el sistema de control de versiones

Como comentario, anota en su registro:

- Comentario 3 :: Se ve mejor y más expresivo exigiendo un lugar para hospedar
                  el amor. ¿Dónde lo alojaré?

*** Tarea: leerlo completo

- url :: https://www.culturagenial.com/es/poemas-de-gabriela-mistral/

* Conozcamos los principales comandos de Git

** Iniciar un proyecto

Seguiremos el ejemplo literario, conoceremos más de los textos preferidos por
nuestro grupo de estudio.

¿Tenemos todos nuestros textos?

Vamos a la terminal y creamos en tu carpeta de ejercicios de la tercera clase
una carpeta llamada *mi_seleccion_literaria*, ¿No será mejor
*nuestra_seleccion_literaria*? ¡A votar!

Haremos, por analogía, un sistema de control de versiones en nuestro cuaderno,
paso a paso. En la columna *Bitáctora* está el título o señal que indica lo que
pondrás en tu cuaderno.

|---------+------------------+----------|
| Acción  | Bitácora         | Git      |
|---------+------------------+----------|
| Iniciar | Iniciar Proyecto | git init |
|---------+------------------+----------|

Todo lo que sea comando ~git~ se debe hacer *precisamente* en la carpeta en que
estás gestionan tu proyecto, en este caso en *mi_seleccion_literaria*.

** Crear un archivo de guia para humanos

Esto es una regla recomendada para hacer accesible para *humanos* nuestro
trabajo. 

Crear un archivo llamado:

- README
- LEAME
- README.org
- README.md

Puedes escoger pero ser coherente con lo que elijas.

** Contenido del archivo guia para humanos

Este archivo debe contener en palabras una explicación respondiendo *¿en qué
consiste este proyecto?* Así como también explicar la estructura de directorios
y archivos que presenta.

*** Ejercicio

Reflexionar y proponer, en grupos de a 2 una estructura de directorios adecuada
a lo que busca el proyecto, crearla y presentarla al curso.


** Registra un comentario en tu cuaderno y en la terminal

Antes de continuar añadiendo nuevo contenido al proyecto es recomendable hacer
un comentario en tu cuaderno, registrando el inicio del proyecto y también desde
la terminal registrar un primer punto o paso.

|------+------------------------------+----------------|
| Paso | Acción en bitácora           | Git            |
|------+------------------------------+----------------|
|    0 | Revisar estado del proyecto  | git status     |
|    1 | Añadir nuevas modificaciones | git add README |
|    2 | Revisar estado del proyecto  | git status     |
|    3 | Comentar avance              | git commit     |
|    4 | Revisar estado del proyecto  | git status     |
|------+------------------------------+----------------|

Al ejecutar el comando, se iniciará un editor que tiene el registro de las
modificaciones al que tendrás que escribirle un comentario.

** Alternativa de comentario compacta

Utilizar *git commit* es la forma general de realizar un registro de avances de
un proyecto. Sin embargo, algo mas compacto es añadir el mismo comentario desde
la terminal (si es que el comentario es algo corto).


|--------------------+--------------------------------------|
| Acción en bitácora | Git                                  |
|--------------------+--------------------------------------|
| Comentar avance    | git commit -m 'Comentario de avance'  |
|--------------------+--------------------------------------|


* Herramientas útiles para la gestion del proyecto

** Analizar los cambios entre una versión y otra

Realizaremos lo siguiente:

1.- Incluiremos un primer texto de tu selección de *poesía*
2.- Revisaremos las diferencias entre el último comentario y lo que estamos añadiendo
3.- Registraremos los cambios en tu cuaderno y en la terminal

Revisar las diferencias.

#+BEGIN_SRC bash
git diff 
#+END_SRC

Permite verificar de manera general los cambios o modificaciones. Sin embargo,
para un análisis particular sobre los cambios en un archivo.

#+BEGIN_SRC bash
git diff archivo
#+END_SRC

- url uso git diff :: https://www.atlassian.com/git/tutorials/saving-changes/git-diff

** Interfaz gráfica para analizar las diferencias

Al mismo tiempo, como ayuda gráfica a *git diff*, es posible utilizar el
programa *meld*, que se instala

#+BEGIN_SRC bash
sudo apt install meld
#+END_SRC

Y luego se utiliza con ~git~

#+BEGIN_SRC bash
git difftool --tool=meld
#+END_SRC


* Cerrando la clase

** Tareas para próxima clase 

Practicar en casa con los siguientes dos textos de *poesía*. Comentando cada
modificación y registrando en el ~README~ las inclusiones para la lectura humana.

** Fin de clase

Guarda tu comandos y practica en casa!

#+BEGIN_SRC bash
history | grep -A 1000 "CLASE_03" > 
historia/clase_numero.sh
#+END_SRC
