archivo="data.csv"
if [ -r "$archivo" ]; then
	size=$(stat --printf="%s" $archivo)
	creacion=$(stat --printf="%y" $archivo)
	echo "El archivo "$archivo" existe y tiene un tamaño de "$size" bytes"
	echo "Además fue modificado por última vez en "$creacion
else
	echo "El archivo "$archivo" no existe"
fi
