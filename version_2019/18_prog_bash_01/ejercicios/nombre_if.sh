read -p "Ingresa tu nombre" nombre
regex="^[[:upper:]][[:lower:]]+$"
match=$(echo $nombre|grep -P $regex)
if [ -n "$match" ]
then
    echo "Tu nombre está bien escrito "${nombre}
else
    echo "Tu nombre, "${nombre}", está mal escrito"
fi
