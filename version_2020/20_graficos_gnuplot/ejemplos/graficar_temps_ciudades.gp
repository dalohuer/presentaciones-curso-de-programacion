set xlabel "Día"
set ylabel "Temperatura [°C]"
set datafile separator ","
set style data histograms

set yrange [0:40]
set xrange [0:8]
set title "Comparación de temperaturas Stgo vs Pto Montt"
set xtics rotate by -45
set style fill solid 0.5
set boxwidth 0.5 relative
set term png
set output "temp_ciudades_comp.png"
plot "temps_ciudades.csv" using 3 with histogram title "Temp. Santiago" lt rgb "#406090", \
     "" using 4:xtic(2) with histogram title "Temp. Pto Montt" lt rgb "#40FF00"
