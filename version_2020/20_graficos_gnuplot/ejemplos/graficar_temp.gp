set xlabel "Día"
set ylabel "Temperatura [°C]"
set datafile separator ","
set yrange [0:50]
set title "Temperaturas de la semana"
set xtics rotate
set style fill solid 0.5
set boxwidth 0.5 relative
set term png
set output "temperaturas.png"
plot "temperaturas.csv" using 0:3:xticlabels(2) with boxes title "temperatura"
