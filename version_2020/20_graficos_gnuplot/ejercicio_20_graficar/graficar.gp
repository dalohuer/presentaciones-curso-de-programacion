set xlabel "Palabras"
set ylabel "Cantidad"
set datafile separator ","
set title "Cantidad de Palabras en Cuentos"
set xtics rotate by -45
set style fill solid 0.5
set boxwidth 0.4 relative
set offsets 0,0,1,0
set grid
plot "seleccion.csv" using 2:xtic(1) with histogram title "Amorosa" lt rgb "#406090",\
     "" using 3:xtic(1) with histogram title "¡Adiós!" lt rgb "#402200",\
     "" using 4:xtic(1) with histogram title "Aparición" lt rgb "#10AA00",\
     "" using 5:xtic(1) with  histogram title "Abandonado" lt rgb "#0044AA",\
    "" using 6:xtic(1) with  histogram title "Antón" lt rgb "#00CC33",\
    "" using 7:xtic(1) with  histogram title "Ahogado" lt rgb "#33CC00",\
    "" using 8:xtic(1) with  histogram title "A caballo" lt rgb "#CC0333",\
    "" using 9:xtic(1) with  histogram title "Amor" lt rgb "#EEAA00"