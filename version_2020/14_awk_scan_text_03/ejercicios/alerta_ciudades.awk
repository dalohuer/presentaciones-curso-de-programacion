@include "funciones.awk"
BEGIN{
  vPAIS=1;
  vCIUDAD=2;
  vTEMP=3;
  FS=",";
  OFS=";";
  print "país", "ciudad", "temperatura °[F]"
}
(NR>1){
  FAR=celsius2farenheit($vTEMP);
  CIUDAD=alerta($vCIUDAD,FAR);
  print $vPAIS,CIUDAD,FAR
}

