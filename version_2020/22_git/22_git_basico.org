#+LATEX_COMPILER: lualatex
#+TITLE: Conocimientos iniciales de un sistema controlador de versiones: git básico, I
#+AUTHOR: Raquel Bracho, David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:t num:t
#+OPTIONS: ^:nil
#+latex_header: \usepackage{setspace}
#+latex_header: \usepackage{fontspec}
#+latex_header: \onehalfspacing


* Temario de Clases

** ¿Qué veremos en esta clase?

- Aprender que es controlar versiones de un proyecto ::
Cómo gestionar un proyecto que está contenido en un directorio, que comprende
archivos y carpetas.

- Aprender de literatura ::
De la selección global de todos los compañeros y compañeras hablaremos sobre
nuestros gustos literarios y textos escogidos.

- Aprender git ::
Es una herramienta de software que, mediante un método relativamente sencillo,
permite controlar versiones de proyectos incluso cuando hay muchas personas
participando de él.

* Registrar Inicio de Clases
** Una marca en la línea de comandos

Todas las clases deberemos registrar el inicio de clases para resguardar y
tener almacenadas nuestras acciones.

Usaremos el comando ~echo~ para dejar una marca

#+BEGIN_SRC bash
echo "CLASE_22::git"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_22*, también podrías anotar el
número de la lista en ~history~.

* El sistema para controlar versiones git

** ¿Qué es git?

#+CAPTION: Mr Robot explica ~git~
#+NAME:   fig: hackerman
#+ATTR_HTML: width="150px"
#+ATTR_ORG: :width 150
#+attr_latex: :width 150px
[[file:./img/git_hackerman.png]]


** ¿Qué es “control de versiones”?


Cuando *gestionamos* (editamos, subimos, eliminamos) cambios sobre los elementos 
de algún proyecto o configuración específica. 

Es como actualizar código al añadir los cambios que creemos convenientes 
y luego subirlos a una plataforma de repositorios.


#+CAPTION: Control de versiones
#+NAME:   fig: control_versiones_simple
#+ATTR_HTML: width="200px"
#+ATTR_ORG: :width 200
#+attr_latex: :width 200px
[[file:./img/control_versiones_simple.png]]


** Git es como una bitácora de trabajo

Es así, si tenemos un proyecto con el siguiente archivo, que la escritora
/Gabriela Mistral/ está escribiendo *Amor amor*.

#+BEGIN_EXAMPLE
Anda libre en el surco, bate el ala en el viento,
late vivo en el sol y se prende al pinar.
No te vale olvidarlo como al mal pensamiento:
¡le tendrás que escuchar!
#+END_EXAMPLE

Desde nuestra herramienta, podemos decir que el proyecto /Poema *Amor amor*/
está en una primera versión.

Entonces, la escritora decide registrar en su cuaderno de notas o /bitácora/, lo
que hizo. Ella puso.

- Comentario 1 :: Ya escritos los primeros versos de *Amor amor*, creo que puedo
                  encontrar más palabras a esto que me quema las entrañas.


** TODO Git trabaja sobre las diferencias entre un paso y otro

Luego de un rato, escribe otros cuatro versos:

#+BEGIN_EXAMPLE
Habla lengua de bronce y habla lengua de ave,
ruegos tímidos, imperativos de mar.
No te vale ponerle gesto audaz, ceño grave:
Grazna profundo en su nido
#+END_EXAMPLE

Y la escritora, talentosa ella, escribe como segundo comentario.

- Comentario 2 :: Siento que este poema está logrando expresar sonantemente los
                  sentimientos que han nacido desde mi corazón. ¿Qué dirá el
                  ave? 

Entonces, nuestra talentosa poeta ha dejado registro de su trabajo, al que
podemos acceder a su evolución-

Aquí el sistema ~git~ detectaría que se han añadido /4 nuevas líneas/ al archivo.

** Git detecta cambio en líneas que ya existen: correcciones literarias.

Nuestra escritora reflexiona sobre el texto, considera que el último verso
escrito no calza con lo que busca, por lo que decide hacer un cambio.

#+BEGIN_EXAMPLE
Habla lengua de bronce y habla lengua de ave,
ruegos tímidos, imperativos de mar.
No te vale ponerle gesto audaz, ceño grave:
¡lo tendrás que hospedar!
#+END_EXAMPLE

¡Ahora parece que si está funcionando bien el texto!

** Creamos un nuevo comentario para el sistema de control de versiones

Como comentario, anota en su registro:

- Comentario 3 :: Se ve mejor y más expresivo exigiendo un lugar para hospedar
                  el amor. ¿Dónde lo alojaré?

*** Tarea: leerlo completo

- url :: https://www.culturagenial.com/es/poemas-de-gabriela-mistral/

* Conozcamos los principales comandos de Git

** Iniciar un proyecto

Seguiremos el ejemplo literario, conoceremos más de los textos preferidos por
nuestro grupo de estudio.

¿Tenemos todos nuestros textos?

Vamos a la terminal y creamos en tu carpeta de ejercicios de la tercera clase
una carpeta llamada *mi_seleccion_literaria*, ¿No será mejor
*nuestra_seleccion_literaria*? ¡A votar!

Haremos, por analogía, un sistema de control de versiones en nuestro cuaderno,
paso a paso. En la columna *Bitáctora* está el título o señal que indica lo que
pondrás en tu cuaderno.

|---------+------------------+----------|
| Acción  | Bitácora         | Git      |
|---------+------------------+----------|
| Iniciar | Iniciar Proyecto | git init |
|---------+------------------+----------|

Todo lo que sea comando ~git~ se debe hacer *precisamente* en la carpeta en que
estás gestionan tu proyecto, en este caso en *mi_seleccion_literaria*.

** Crear un archivo de guia para humanos

Esto es una regla recomendada para hacer accesible para *humanos* nuestro
trabajo. 

Crear un archivo llamado:

- README
- LEAME
- README.org
- README.md

Puedes escoger pero ser coherente con lo que elijas.

** Contenido del archivo guia para humanos

Este archivo debe contener en palabras una explicación respondiendo *¿en qué
consiste este proyecto?* Así como también explicar la estructura de directorios
y archivos que presenta.

*** Ejercicio

Reflexionar y proponer, en grupos de a 2 una estructura de directorios adecuada
a lo que busca el proyecto, crearla y presentarla al curso.


** Registra un comentario en tu cuaderno y en la terminal

Antes de continuar añadiendo nuevo contenido al proyecto es recomendable hacer
un comentario en tu cuaderno, registrando el inicio del proyecto y también desde
la terminal registrar un primer punto o paso.

|------+------------------------------+----------------|
| Paso | Acción en bitácora           | Git            |
|------+------------------------------+----------------|
|    0 | Revisar estado del proyecto  | git status     |
|    1 | Añadir nuevas modificaciones | git add README |
|    2 | Revisar estado del proyecto  | git status     |
|    3 | Comentar avance              | git commit     |
|    4 | Revisar estado del proyecto  | git status     |
|------+------------------------------+----------------|

Al ejecutar el comando, se iniciará un editor que tiene el registro de las
modificaciones al que tendrás que escribirle un comentario.

** Alternativa de comentario compacta

Utilizar *git commit* es la forma general de realizar un registro de avances de
un proyecto. Sin embargo, algo mas compacto es añadir el mismo comentario desde
la terminal (si es que el comentario es algo corto).


|--------------------+--------------------------------------|
| Acción en bitácora | Git                                  |
|--------------------+--------------------------------------|
| Comentar avance    | git commit -m 'Comentario de avance'  |
|--------------------+--------------------------------------|


* Herramientas útiles para la gestion del proyecto

** Analizar los cambios entre una versión y otra

Realizaremos lo siguiente:

1.- Incluiremos un primer texto de tu selección de *poesía*
2.- Revisaremos las diferencias entre el último comentario y lo que estamos añadiendo
3.- Registraremos los cambios en tu cuaderno y en la terminal

Revisar las diferencias.

#+BEGIN_SRC bash
git diff 
#+END_SRC

Permite verificar de manera general los cambios o modificaciones. Sin embargo,
para un análisis particular sobre los cambios en un archivo.

#+BEGIN_SRC bash
git diff archivo
#+END_SRC

- url uso git diff :: https://www.atlassian.com/git/tutorials/saving-changes/git-diff

** Interfaz gráfica para analizar las diferencias

Al mismo tiempo, como ayuda gráfica a *git diff*, es posible utilizar el
programa *meld*, que se instala

#+BEGIN_SRC bash
sudo apt install meld
#+END_SRC

Y luego se utiliza con ~git~

#+BEGIN_SRC bash
git difftool --tool=meld
#+END_SRC


* Moverse entre versiones comentadas

** Un comic para /entender/ git

#+NAME:   fig: git_time_machine
#+ATTR_HTML: width="300px"
#+ATTR_ORG: :width 300
#+attr_latex: :width 300px
[[file:./img/git_time_machine.png]]

** El concepto de /checkout/

La acción *git checkout punto_o_rama* permite al usuario moverse entre los
distintos puntos registrados durante el proyecto.

Es decir, reconstruye el sistema de archivos y directorios tal como quedó
registrado en ese momento.

Primero debemos *conocer* el valor identificador del commit o registro al que
deseamos saltar.

#+BEGIN_SRC bash
git log --pretty=oneline
COMMIT_ID="VALOR_ID_COMMIT"
#+END_SRC

** Moverse a un COMMIT_ID

Luego, copiamos el /string/ identificador del commit y nos movemos con
*checkout*

#+BEGIN_SRC bash
git checkout $COMMIT_ID
#+END_SRC

Ahora, el proyecto toma la forma y contenidos al momento de hacer ese registro.

** Ejercicio con commit y checkout

Añadir un segundo texto. Que sea un *cuento*

Terminar de añadir el primer texto de *poesía*, por cada párrafo añadido
realizar un commit.

Una vez terminado el registro del texto. Moverse al principio, al medio y al
final de la cadena total. Esto se hace utilizando el set de comandos ~git~ ta aprendidos.

* Deshacer cambios equivocados antes del commit

** En caso de error en tu texto 

Hagamos el caso de escribir un texto con errores, muchos errores, pero nos
gustaría volver a la versión inicial del ~commit~ anterior.

Conociendo el *archivo con error*, podemos hacer un /checkout/ hasta el punto
anterior.

#+BEGIN_SRC bash
git checkout -- <archivo-con-error>
#+END_SRC

Importante las *--* que indican hacer el checkout hasta el punto anterior.

* Un repositorio web para almacenar el proyecto
  
** Crear un usuario de la plataforma gitlab

Ingresar al sitio *www.gitlab.com* y registrarse como usuario.

#+NAME:   fig: git_time_machine
#+ATTR_HTML: width="250px"
#+ATTR_ORG: :width 250
#+attr_latex: :width 250px
[[file:./img/registro_gitlab.png]]

Una vez registrado, el siguiente paso es estudiar la interfaz. 

** Crear un proyecto *selección literaria*

Iniciar un *nuevo proyecto* en el botón verde *new project*, con opción /Public/

#+NAME: fig: nuevo_proyecto
#+ATTR_HTML: width="250px"
#+ATTR_ORG: :width 250
#+attr_latex: :width 250px
file:./img/nuevo_proyecto.png

** Asociar repositorio a proyecto en computador

Luego, tenemos que copiar los siguientes comandos (desde /git remote .../) y
subir el proyecto.

#+NAME: fig: nuevo_proyecto
#+ATTR_HTML: width="250px"
#+ATTR_ORG: :width 250
#+attr_latex: :width 250px
file:./img/asociar_repo.png


* Herramientas útiles para la gestion del proyecto

** Utilizar una interfaz gráfica de ayuda

De manera gráfica, se pueden instalar diversos programas llamados *Clientes git
gráficos*, que te serán de utilidad ya que visualmente son una guía para
gestionar el proyecto.

- url :: https://git-scm.com/download/gui/windows

Desde la terminal instala 

#+BEGIN_SRC bash
sudo apt install git-gui
#+END_SRC

* Subir el proyecto a la web 

** Subir un proyecto o actualizaciones de este

Cada vez que se termine de realizar un *commit* o registro, si deseamos
preservar en un repositorio el proyecto deberíamos hacer

#+BEGIN_SRC bash
git push
#+END_SRC

De manera análoga, en caso necesario, podemos determinar el *origen* y *destino*, podemos usar

#+BEGIN_SRC bash
git push -u origin master
#+END_SRC

Luego, podemos usar el comando *git push* de forma normal.

* Hacer una versión publicable

** Publica una versión descargable del proyecto 

Cada ciertos ~commits~ el grupo de trabajo estimará que el proyecto tiene un
/aura/ interesante de ser publicable.

Le podemos decir al sistema ~git~  que, en cierto ~commit~ empaquete el proyecto y
lo anote como un ~tag~.

#+BEGIN_SRC bash
git tag -a v1.4 -m 'my version 1.4'
#+END_SRC

Si quieres revisar las /etiquetas/ ya existentes, basta con

#+BEGIN_SRC bash
git tag
#+END_SRC

** Enlace del tag en la plataforma

Primero, deberíamos hacer un ~git push~ de la versión etiquetada. Esta versión
es un paquete que contiene el contenido actual sin la parte /invisible/ del
sistema de gestión de versiones.

#+BEGIN_SRC bash
git push origin v1.4
#+END_SRC

Por ejemplo, en la plataforma ~gitlab~, encontrarás la lista de ~tags~ en (para
las presentaciones de este curso)

- url :: https://gitlab.com/pineiden/presentaciones-curso-de-programacion/-/tags

* Comparte el proyecto 

** Pon el link de tu proyecto en el chat con un comentario

En el chat de *Telegram del curso* comparte el enlace con tus compañeros.

- ¿Cuáles proyectos descargarías directamente mediante su ~tag~?
- ¿Cuáles clonarías?

** Descarga otros proyectos de tus compañeros

Descarga otros dos proyecto, clonando los proyectos de tus compañeras/os.

Si uno de ellos de da la ~url~  "url_seleccion_literaria", lo puedes hacer
escribiendo

#+BEGIN_SRC bash
git clone url_seleccion_literaria
#+END_SRC

** Descubrir proyectos de software en gitlab

Buscar, con tu grupo de trabajo, algún proyecto dentro de la plataforma gitlab,
investigar su estructura de carpetas, los tipos de archivos que utiliza, los
manuales y la documentación.

* Aporta con ideas a un proyecto de otro grupo

** ¿Quieres aportar de manera sencilla?

Si estás participando de un proyecto en que encuentras que puedes aportar, por
alguna razón como

- error en alguna parte del proyecto
- consulta sobre como se usa o lee algo
- sugerencias de como hacer mejor algo
- ideas de nuevas características

** Un sistema de foro de reportes

Existe una sección de ~issues~ o (temas) que se pueden ir reportando en la
plataforma.

Por ejemplo, para el proyecto que contiene el código de la plataforma que hace
~gitlab~, es:

- url ::  https://gitlab.com/gitlab-org/gitlab-ee/issues

         
* Cerrando la clase

** Tareas para próxima clase 

Investigar y ejercitar con git. 

- ¿Cómo crear una rama git?
- ¿Cómo omitir algún archivo o conjunto de archivos?

Ejercicios de git:

- url 1 :: http://aprendeconalf.es/git/ejercicios/
- youtube :: [[https://www.youtube.com/watch?v=zH3I1DZNovk&list=PL9xYXqvLX2kMUrXTvDY6GI2hgacfy0rId][Git codigofacilito]]
- libro :: https://git-scm.com/book/en/v2
** Fin de clase

Guarda tu comandos y practica en casa!

#+BEGIN_SRC bash
history | grep -A 1000 "CLASE_22" > 
historia/clase_22.sh
#+END_SRC

