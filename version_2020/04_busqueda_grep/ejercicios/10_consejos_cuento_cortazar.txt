Decálogo para escribir un cuento
===============================


1. No existen leyes para escribir un cuento, a lo sumo puntos de vista.

2. El cuento es una síntesis centrada en lo significativo de una historia.

3. La novela gana siempre por puntos, mientras que el cuento debe ganar por knock-out.

4. En el cuento no existen personajes ni temas buenos o malos, existen buenos o malos tratamientos.

5. Un buen cuento nace de la significación, intensidad y tensión con que es escrito; del buen manejo de estos tres aspectos.

6. El cuento es una forma cerrada, un mundo propio, una esfericidad.

7. El cuento debe tener vida más allá de su creador.

8. El narrador de un cuento no debe dejar a los personajes al margen de la narración.

9. Lo fantástico en el cuento se crea con la alteración momentánea de lo normal, no con el uso excesivo de lo fantástico.

10. Para escribir buenos cuentos es necesario el oficio del escritor.

