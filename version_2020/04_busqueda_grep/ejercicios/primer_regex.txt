Nuestro primer expresión regular
===============================


Esa vez que mi mamá me dijo que necesitaba leer unos libros muy extraños
sobre la biología extraterrestre de los vegetales de Júpiter, lo dudé un instante.
Mi papá era el loco, mi madre siempre era la centrada.
No sabía lo que pasaba, por lo que decidí contarles a mis hermanos sobre nuestra mamá 
y decidimos llevarla al médico.
Mi papá se enteró cuando ibamos saliendo, así que decidió acompañarnos.
Mientras esperabamos en la consulta para que la atendieran, sucedió un corte de luz,
estabamos a oscuras y de repente un resplandor que nos enceguece. Volvió la luz y mis 
mamá había desaparecido, mi padre lloró.
