#+LATEX_COMPILER: lualatex
#+TITLE: Introducción a redes y SSH
#+AUTHOR: David Pineda Osorio, Camilo Carrasco
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:1 num:f
#+OPTIONS: ^:nil
#+latex_header: \usepackage{setspace}
#+latex_header: \usepackage{fontspec}
#+latex_header: \onehalfspacing

* Temario de la clase

** ¿Qué veremos en esta clase?

- comunicación en red :: nociones básicas de dirección ip, puertos, modelo OSI.
  
- conceptos básicos :: aclarar las ideas de cliente/servidor.
  
- acceso remoto :: realizar conexiones a equipos remotos.

- copiar remoto :: acciones de copia entre equipos en red.

- sincronizacion de directorios :: uso de herramientas para respaldo  
  
   
* Registrar inicio de clases

  Para poder realizar una búsqueda que tenga un mayor contexto y precisión, 
es posible añadir más texto a la seña que nos permita mejorar la referencia.

Recomiendo añadir, al número de la clase, la seña relacionada al nombre de la
clase, que en este caso sería *ssh*.

#+BEGIN_SRC bash
echo "CLASE21::ssh"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_21::ssh*

* Comunicación en red

** Una red interconectada

En el mundo real los computadores están interconectectados mediante sistemas de
comunicación a diversas redes. Mediante la instauración de diversos protocolos
estándar se hace posible que un computador se pueda comunicar con uno o varios a
la vez.

Cuando la red es de acceso limitado o regulado por un acceso se le puede llamar
/Intranet/, cuando es abierta y de acceso ilimitado (hasta cierto punto) se le
llama /Internet/. En términos de comunicación será indiferente una de otra, ya
que en ambas es posible ocupar las mismas herramientas para entregar mensajes
entre equipos.

[[file:./img/internet.jpg]]

La base de la comunicación entre un punto y otro es que cualquier elemento de
información en uno puede ser /serializado/ en una *cadena de bytes* que, al
llegar al otro punto vuelve a ser transformado en el elemento. En realidad es
una copia. Por ejemplo, cuando visitas una página web o descargas un archivo lo
que haces es solicitar

Por ejemplo, para enviar el texto
#+NAME: serialize
#+BEGIN_SRC bash :results output :exports both
echo "Hola, ¿Cómo estás?"|xxd -p
#+END_SRC

Debemos transformarlo en una serie de bytes, que serían:

#+RESULTS: serialize
: 486f6c612c20c2bf43c3b36d6f20657374c3a1733f0a

La cadena en binario (que es finalmente lo que se envía), será:

#+NAME: ser2
#+BEGIN_SRC bash :results output :exports both
 echo "Hola, ¿Cómo estás?"|xxd -p  -b|awk '{
      for(i=2;i<NF;i++){
      if($i ~ /^[01]+$/){printf $i" "} }
      print""}'
#+END_SRC

Resultando lo siguiente:

#+RESULTS: ser2
: 01001000 01101111 01101100 01100001 00101100 00100000 
: 11000010 10111111 01000011 11000011 10110011 01101101 
: 01101111 00100000 01100101 01110011 01110100 11000011 
: 10100001 01110011 00111111 00001010 

Ahora bien, para distinguir un computador de otro se han implementado soluciones
similares a la definición de una dirección. En efecto, cada computador que se
conecta a una red tendrá una *dirección IP*.

Cada vez que necesitemos acceder a alguna información que contenga deberemos
generar una conexión a esa dirección y obtenerla si se tienen las autorizaciones suficientes.

* Conceptos básicos de comunicación

** Direcciones IP y nombres de dominio

Una dirección IP es una serie de *cuatro* números del 0 al 255 separadas por
".". Esto para el estándar IPv4. Para el estándar más nuevo, IPv6, la serie de
números aumenta. Nos enfocaremos en IPv4.

Cuando digitamos una dirección en el navegador, como
*www.cursodeprogramacion.cl* estamos en realidad llamando a una dirección IP que es
*45.55.45.60*.

Las IP pueden dividirse en dos grandes grupos:

- públicas :: de acceso abierto
- privadas :: de acceso restringido
  
Cuando usamos un *nombre de dominio* como el que acabamos de usar, entonces la
ip tiene asociado un nombre (*DNS*), También pueden haber varios *DNS* asociados
a la misma IP.

** Protocolos de comunicación

Para comunicar entre dos o más puntos será necesario conocer los protocolos base
de comunicación.

- TCP :: permite comunicación con bajo nivel de errores pero cada paquete es muy
  grande, por el encabezado y los *ack* o elementos de respuesta y sincronización.

- UDP :: es un tipo de comunicación definida para emitir y comunicar paquetes
  rápidamente pero no tiene control de pérdida de paquetes.

** Modelo OSI

Para comprender como opera una red y se hace posible comunicar diferentes
terminales y nodos, será necesario tener en cuenta la existencia de los llamados
*Modelo OSI y TCP* que dan una intepretación de cómo existe un sistema de
comunicación en read.

[[file:./img/osi_tcp.png]]

Con estos modelos análogos podemos observar que una red se compone de diferentes
niveles. Cada nivel tiene una función específica y depende de la inferior.

- nivel físico :: es la red concreta, los equipos, cables, fibras y
  comunicaciones de radio.

- nivel enlace :: es el conjunto de direcciones asignadas que permiten
  identificar cada equipo.

- nivel red ::   Define el tipo de red a la que pertenece el sistema

- nivel transporte :: define la forma en que se comunicará, crea las conexiones,
  comunica, etc.

- nivel aplicación :: es la capa de software en que se activan distintas
  utilidades para la comunicación.

** Puertos de conexión

En la comunicación de red, existe la idea de *PUERTO*. Es decir, es un número
(del 0 al 65535) al que distintos servicios disponen de sus conexiones. Cada
servicio puede activar uno o más puertos según su configuración.

Cuando ponemos en el navegador *www.cursodeprogramación.cl*, estamos usando el
protocolo *http* y este se conecta mediante el puerto 80. Es lo mismo que
escribir *www.cursodeprogramación.cl:80*.

Una vez que el servidor reconoce la *url* verifica que la configuración del
sistema está en modo seguro, por lo que debe cambiar el protocolo a *http* y el
puerto que ocupa será *443*.

Otros servicios, como ftp usualmente usa el puerto *20*, postgresql *5432* y
así con cada caso.

Distintos servicios no pueden ocupar el mismo puerto.

- puertos conocidos :: todos los inferiores a 1024 son de uso común, para
  protocolos conocidos.
- puertos registrados :: entre 1025 y 49151 se pueden usar por diferentes aplicaciones.
- puertos privados o dinámicos :: hasta el 65535 se asignan a los clientes al
  conectarse

** Socket

Socket es un elemento informático que permite la conexión entre dos procesos.
Estos procesos pueden estar en el mismo computador o conectados mediante alguna
red.

Permiten lo que se llama la Interconexión de Procesos o (IPC).

Existen básicamente dos tipos importantes:

- Unix socket :: opera como un archivo y sirve para intercambiar información
  entre programas

- TCP/IP socket :: opera como punto de conexión entre diferentes terminales de
  una red.

Se debe diferenciar dos estados principales:

- servidor :: modo en el cual el socket *escucha* conexiones y provee la
  información que genera un computador.

- cliente :: modo en que un terminal se conecta a un *servidor* y este le
  entrega la conexión de ida y de vuelta.  

** Ping :: revisar comunicación

El comando *ping* permite entregar un información de si es posible conectarse a
una url, lo hace enviando un mensaje de prueba y recive otro. Calcula el timepo
que demora en voler. 
   
Dada una lista de *urls* buscaremos:

- www.cursodeprogramacion.cl
- www.wikipedia.org
- www.youtube.com

Haremos un ping por cuatro veces.

#+NAME: ping
#+BEGIN_SRC bash :results output :exports both
ping -c 4 www.cursodeprogramacion.cl
#+END_SRC

Nos resulta con la siguiente salida.

#+RESULTS: ping
: PING www.cursodeprogramacion.cl (45.55.45.60) 56(84) bytes of data.
: 64 bytes from quux (45.55.45.60): icmp_seq=1 ttl=46 time=188 ms
: 64 bytes from quux (45.55.45.60): icmp_seq=3 ttl=46 time=186 ms
: 64 bytes from quux (45.55.45.60): icmp_seq=4 ttl=46 time=199 ms
: 
: --- www.cursodeprogramacion.cl ping statistics ---
: 4 packets transmitted, 3 received, 25% packet loss, time 31ms
: rtt min/avg/max/mdev = 185.800/190.780/198.743/5.700 ms
  
** Traceroute

Traceroute es, como podemos deducir de su nombre, un comando que entrega la ruta
por todos los puntos por los que pasa un mensaje para llegar a destino.

Primero, lo instalamos.

#+BEGIN_SRC bash
sudo apt install traceroute
#+END_SRC

Luego, probamos para el mismo set de direcciones.

#+NAME: traceroute
#+BEGIN_SRC bash :results output :exports both
traceroute www.cursodeprogramacion.cl
#+END_SRC

Resulta en la siguiente lista de direcciones. Que es precisamente por donde pasa
un mensaje que se comunica entre dos puntos.

#+RESULTS: traceroute
#+begin_example
traceroute to www.cursodeprogramacion.cl (45.55.45.60), 30 hops max, 60 byte packets
 1  _gateway (192.168.43.117)  13.812 ms  13.804 ms  13.793 ms
 2  * * *
 3  * * *
 4  172.25.238.90 (172.25.238.90)  73.187 ms 172.25.238.117 (172.25.238.117)  74.267 ms 172.25.238.79 (172.25.238.79)  74.479 ms
 5  * * *
 6  * * *
 7  10.170.115.206 (10.170.115.206)  59.050 ms  24.251 ms 10.170.115.202 (10.170.115.202)  28.921 ms
 8  192.168.105.158 (192.168.105.158)  43.810 ms  47.675 ms  77.536 ms
 9  172.31.215.101 (172.31.215.101)  65.942 ms  80.775 ms *
10  172.31.198.6 (172.31.198.6)  213.720 ms  207.192 ms  183.296 ms
11  172.31.214.185 (172.31.214.185)  70.620 ms  73.669 ms  73.626 ms
12  190.211.160.4 (190.211.160.4)  100.158 ms  99.607 ms  99.576 ms
13  de-cix.nyc.internexa.com (206.82.104.201)  253.438 ms  250.667 ms  250.814 ms
14  * * *
15  138.197.244.41 (138.197.244.41)  201.071 ms 138.197.244.33 (138.197.244.33)  198.150 ms 138.197.244.35 (138.197.244.35)  200.243 ms
16  * * *
17  * * *
18  * * *
19  * * *
20  * * *
21  * * *
22  * * *
23  * * *
24  * * *
25  * * *
26  * * *
27  * * *
28  * * *
29  * * *
30  * * *
#+end_example

Es una herramienta que sirve para estudiar la comunicación entre dos puntos.
A veces, al momento de elegir, será necesario seleccionar aquel servidor más
cercano o que tenga menos saltos entre puntos.

** Protocolos   
   
Ya hemos hablado un poco del concepto *protocolo*. Esto quiere decir que en
informática, para comunicar dos puntos se debe establecer *la forma* en que un
dato es enviado de un punto a otro.

En principio ya sabemos que la base es una *serie de bytes*. Ahora para saber el
modo en que van ordenados y a qué se refiere la información, será necesario
establecer unas tablas de *decodificación*.

Las tablas de *decodificación* en su conjunto establecen un *protocolo*, que
dado cierto encabezado de bytes (o trozo inicial), será posible reconocer la
información que comunica el resto.

Cada uno de estos protocolos se debe configurar, para que escuche conexiones en
algún puerto específico. 

Así, diferentes protocolos existen para diferentes usos.

- http, 80
- https, 443
- ftp, 20
- postgresql,5432
- rabbitmq,5672
- ssh, 22

Los valores de los puertos son por defecto, pueden cambiarse.

** Comunicación en limpio

Cuando comunicamos en limpio permitimos que cualquier otro dispositivo pueda
tomar la comunicación, leer y descubrir de qué se trata.

Al usar *http* se realiza comunicación en limpio o /transparente/. No es
recomendable usar este modo al momento de comunicar contraseñas o información
delicada.

Para eso es mejor comunicar con *encriptación*

** Comunicación segura encriptada

Una comunicación encriptada es aquella que *no permite* que otros dispositivos
tomen la comunicación y entienda lo que se está transmitiendo.

Para realizar esto se debe pasar la información por un *proceso de
encriptación*, en que se utilizan diferentes métodos para ocultar la información
y que se posible mostrarla en el punto de llegada.

La *encriptación* es un proceso matemático que depende de la capacidad de
cómputo y de algoritmo creado para tales efectos. Cada cierto tiempo un método
queda obsoleto porque se descubre la forma de cómo descubri el mensaje.

El *protocolo SSH* permite comunicacioń segura y encriptada. Cada cosa que
hagamos está oculta a la vista de otros sistemas.

* Acceso remoto, cliente SSH

SSH es un protocolo de red para realizar inicios de sesión en máquinas remotas
en una red local o por Internet. 

Las arquitecturas SSH generalmente incluyen un servidor SSH, a los cuales se
conectan los clientes SSH a la máquina remota. 

Se puede verificar la version del cliente ssh instalado en el sistema con:

random@maquina:~$ ssh -V
OpenSSH_8.4p1 Debian-3, OpenSSL 1.1.1h  22 Sep 2020

Si obtenemos una salida similar, significa que podemos conectanos a máquinas
remotas como cliente utilizando la utilidad SSH. 

También significa que las utilidades específicas relacionadas con el protocolo
SSH (como scp, por ejemplo) o relacionadas con servidores FTP (como sftp)
estarán disponibles en mi host. 

#+BEGIN_EXAMPLE
ssh usuario@numero_ip
ssh usuario@dominio.com
#+END_EXAMPLE

Si el punto de acceso está habilitado, entonces podrás conectarte. En general te
pedirá la password de usuario para acceder.

* Llaves ssh

Cuando se haga usual que necesites conectarte a otro computador o servidor, por
algún motivo como para respaldar tu información, dejar mensajes secretos o
administrar una web, puedes usar lo que se llaman las llaves ssh.

Estas /llaves/ son en realidad dos archivos en formato *texto*:

- llave privada :: es la llave que solo debes tener en tu computador

- llave pública :: es la llave que puedes colocar en los computadores de uso habitual.  
  
** Crear llaves SSH

Para creara un set de llaves ssh debes tipear.

#+BEGIN_SRC bash
ssh-keygen
#+END_SRC

Para mejorar el nivel de encriptación de la llave, podemos escoger un método
específico de encriptación.

La más recomendada en estos momentos es *Ed25519*, por lo que para cosas
importantes se recomienda usar este generador.

#+BEGIN_SRC bash
ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/id_ed25519 -C "juan@ejemplo.com"
#+END_SRC

Cada opción significa lo siguiente:

- o :: guarda en el nuevo formato de openssh

- a :: el número de derivaciones, mientras mayor más seguro pero más lento.

- t :: tipo de creador de llaves

- f :: nombre de archivo

- C :: comentario, en este caso un correo

Añadimos la llave al conjunto de llaves disponibles.

#+BEGIN_SRC bash
ssh-add ~/.ssh/id_ed25519
#+END_SRC

** Copiar las llaves en un servidor

Para copiar la llave pública en un servidor con *ssh server* puedes hacer lo
siguiente:

- Copiar a mano el contenido de *id_rsa.pub* en *know_hosts*

O bien ejecutar el siguiente comando.

#+BEGIN_SRC bash
ssh-copy-id usuario@direccion_ip
#+END_SRC

Luego de esto, al conectarte, no se pedirá password.  

* Puente a un puerto con SSH

Cuando usamos nuestro computador, la IP propia sera *127.0.0.1* o bien el nombre asignado por defecto será *localhost*.

Si tenemos un servidor (computador remoto) con algún servicio que lo dispone
mediante algún puerto específico disponible. Podemos hacer que, mediante una
llamada, ese puerto remoto esté disponible como un puerto local.

#+BEGIN_src bash
ssh -f -N4 -L 2345:localhost:5432 web@numero_ip
#+END_SRC

Con esto, si el servidor con *numero_ip* dispone de un servicio de base de datos
*postgresql* en el puerto *5432*, ahora nosotros podemos conectarnos
directamente al puerto *2345* de manera local.

#+BEGIN_SRC bash
psql -p 2345
#+END_SRC

Esto es muy útil cuando algunos servicios no disponen de comunicación segura,
por lo que se recomienda gestionar la comunicación con SSH que si va encriptada.

* Acceso remoto con aplicaciones de escritorio

Añadimos la opción $+X$ o $+Y$ para activar aplicaciones visuales.  

#+BEGIN_SRC bash
ssh +Y usuario@ip
#+END_SRC

Luego, podemos levantar aplicaciones como /firefox/ y cualquiera que necesite
/X.org/ o sistema visual.


* FTP seguro: SFTP

También es posible comunicarse con un servidor utilizando protocolo ssh de
manera visual.

Un software recomendado para tal caso es *filezilla*, que permite gestionar
archivos y directorios con una interfaz gráfica.

Instalar filezilla

#+BEGIN_SRC bash
sudo apt install filezilla
#+END_SRC

Uso de filezilla:

- se configura la conexión, dadas la IP, usuario y password. Se puede cambiar el
  puerto, por defecto 22.
  
#+CAPTION: Configurar conexión
#+ATTR_LATEX: :width 300px
#+ATTR_HTML: :width 300px
[[file:./img/ssh_filezilla_conf.png]]

- en la primera conexión, pregunta si estás seguro/a de contectarte y si deseas
  guardar alguna información.

#+CAPTION: Aceptar registro de conexión
#+ATTR_LATEX: :width 300px
#+ATTR_HTML: :width 300px
[[file:./img/ssh_filezilla_primera_con.png]]

- en esta ventana podrás mover o coppiar carpetas o archivos desde tu computador
  al servidor y viceversa.
  
#+CAPTION: Interactuar con archivos y direcotorios
#+ATTR_LATEX: :width 300px
#+ATTR_HTML: :width 300px
[[file:./img/filezilla_conexion.png]]


* Copia remota con scp

Para copiar archivos, conjuntos de archivos o directorios se puede usar *scp*,
un comando que usa *ssh* para comunicar e intercambiar información

- Enviar desde mi computador a servidor

#+BEGIN_SRC bash   
scp origen usuario@ip:destino  
#+END_SRC

- Enviar desde servidor hasta mi computador

#+BEGIN_SRC bash   
scp usuario@ip:origen destino
#+END_SRC

- Copiar entre dos usuarios de dos computadores diferentes.

#+BEGIN_SRC bash      
scp usuario@ip:origen usuario2@ip2:destino
#+END_SRC

* Sincronización de directorios
  
Cuando necesitamos respaldar una serie de archivos o directorios, que van
cambiando progresivamente, puede ser util utilizar *rsync* por sobre *scp*.

El primero presenta la ventaja de que solamente copia el tramo de información o
*bytes* que es nueva al conjunto.

De manera normal *rsync* se usa así:

#+BEGIN_SRC bash
  rsync origen destino
#+END_SRC

Con ssh, debemos decirle con la opción *e* que usaremos ssh.

#+BEGIN_SRC bash
  rsync -avz -e "ssh" usuario@midominio.com:/home/usuario/ ./Respaldos/usuario/
#+END_SRC
  
* Server SSH

** Instalar y habilitar SSH con OpenSSH en Debian 10.

La instalación del programa servidor de SSH, posibilita y gestiona las conexiones de clientes a nuestro computador.

Para instalar OpenSSH, ejecutamos:

#+BEGIN_SRC bash
sudo apt update
sudo apt install openssh-server
#+END_SRC

Para comprobar el estado del demonio de OpenSSH:

#+BEGIN_EXAMPLE
sudo systemctl status sshd
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2020-12-28 13:48:27 -03; 3h 34min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 868 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 918 (sshd)
      Tasks: 1 (limit: 4546)
     Memory: 1.7M
     CGroup: /system.slice/ssh.service
             └─918 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

dic 28 13:48:27 maquina systemd[1]: Starting OpenBSD Secure Shell server...
dic 28 13:48:27 maquina sshd[918]: Server listening on 0.0.0.0 port 22.
dic 28 13:48:27 maquina sshd[918]: Server listening on :: port 22.
dic 28 13:48:27 maquina systemd[1]: Started OpenBSD Secure Shell server.
#+END_EXAMPLE

En el log/registro podemos ver que el servidor de OpenBSD SSH se está iniciando y es puesto en modo escucha en los puertos 22.

De forma predeterminada, el servidor SSH se ejecutará en el puerto 22.

Se puede verificar ejecutando el comando netstat:

#+BEGIN_EXAMPLE
netstat -tulpn | grep ":22"
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp6       0      0 :::22                   :::*                    LISTEN      -                   
#+END_EXAMPLE

Para comprobar que OpenSSH arranque automáticamente al inicio, podemos ejecutar el siguiente comando:

#+BEGIN_EXAMPLE
sudo systemctl list-unit-files | grep enabled | grep ssh
ssh.service                                                               enabled         enabled      
ssh.socket                                                                disabled        enabled      
#+END_EXAMPLE

En caso de no obtener algun valor devuelto, debemos habilitar el servicio con el comando:

#+BEGIN_EXAMPLE
sudo systemctl enable ssh
Synchronizing state of ssh.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable ssh
#+END_EXAMPLE


** Configuracion SSH

De forma predeterminada, los archivos de configuración de SSH se encuentran en 

- /etc/ssh/ssh_config :: Define las configuraciones del cliente SSH
  
- /etc/ssh/sshd_config :: Define las configuraciones del servidor SSH. Por
  ejemplo, para definir el puerto SSH o para implementar multiples factores de
  autenticacion, para que solo los  usuarios autorizados se puedan comunicar con
  el servidor. 

*** Configurar servidor SSH

Se edita el archivo de configuracion del servidor de ssh:

#+BEGIN_SRC bash
sudo nano /etc/ssh/sshd_config
#+END_SRC


Se edita la linea asociada al puerto, descomentandola y cambiando el puerto a alguno que no este tomado por algun otro servicio.

#+BEGIN_EXAMPLE
#Port 22
Port 2222

#PermitRootLogin prohibit-password
PermitRootLogin no
#+END_EXAMPLE

*** Aplicar configuracion

Al cambiar la configuracion en los archivos correspondientes, para hacer efectivas dichas configuraciones es necesario reiniciar el demon de OpenSSH.

#+BEGIN_SRC bash
sudo systemctl restart sshd
sudo systemctl status sshd
#+END_SRC

Se puede comprobar el estado del puerto con:

#+BEGIN_EXAMPLE
netstat -tulpn | grep ":22"
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
tcp        0      0 0.0.0.0:2222            0.0.0.0:*               LISTEN      -                   
tcp6       0      0 :::2222                 :::*                    LISTEN      -                   
#+END_EXAMPLE


