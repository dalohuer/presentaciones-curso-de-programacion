#+LATEX_COMPILER: lualatex
#+TITLE: Bash avanzado: programación de scripts I
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:1 num:f
#+OPTIONS: ^:nil
#+latex_header: \usepackage{setspace}
#+latex_header: \usepackage{fontspec}
#+latex_header: \onehalfspacing


* Temario de la clase

** ¿Qué veremos en esta clase?

- Estructura de control ~if~ :: como programar con condicionales

- Creación de funciones :: cómo crear y utilizar funciones de ~bash~.

- Uso de listas o arrays :: el uso de listas o arreglos de datos.

* Comandos nuevos y útiles

** El uso de ~stat~ para extraer información de un archivo

Se usa el comando ~stat~ para extraer información llamada *metadata* que
describe características del archivo. 

#+NAME: stats
#+BEGIN_SRC shell :results replace output  :session vino :exports both
  archivo="nombres.csv"
  stat ./ejercicios/$archivo
#+END_SRC

Dentro de *ejercicios*, realizar el ejercicio para los archivos *nombres.csv* y *parentesco.csv*.

Resulta una serie de información que está relacionada con el archivo.

#+RESULTS: stats
: 
: Fichero: ./ejercicios/nombres.csv
:   Tamaño: 192       	Bloques: 8          Bloque E/S: 4096   fichero regular
: Dispositivo: fe00h/65024d	Nodo-i: 57019172    Enlaces: 1
: Acceso: (0777/-rwxrwxrwx)  Uid: ( 1000/   david)   Gid: ( 1000/   david)
:       Acceso: 2020-11-19 12:05:53.690336205 -0300
: Modificación: 2018-05-06 14:11:54.346959000 -0300
:       Cambio: 2020-11-16 20:26:57.656822937 -0300
:     Creación: -

¿Cómo podría hacerse para filtrar y obtener el usuario y el grupo asignado?


* El control de flujo con ~if~

  En el lenguaje ~bash~ dispone también de sus estructuras de control, como
  *if*, *for* o *while*. Ya hemos visto como operar de manera básica *for*, que
  sirve para iterar sobre listas de elementos. El condicional *if* nos permitirá
  regular el flujo de un programa en base a ciertas condiciones que se expresan
  en relación a variables, valores o comparaciones.  

** Sentencias lógicas

Recordando, el control de flujo ~if~ permite direccionar el flujo de un programa
dada si una *condición* resulta  ser *verdadera* o *false*.

La estructura ~if~ trabaja en conjunto con una *operación booleana* que entrega
el resultado booleano de la *condición*

En seudocódigo se tiene:

#+BEGIN_EXAMPLE
if (CONDICION), entonces =>
{
Realizar algo si se cumple la CONDICIÓN
CONDICIÓN es una expresión de una operación booleana
Su resultado es Verdadero o Falso 
}
#+END_EXAMPLE

** Estructura del condicional ~if~ en ~bash~

En ~bash~ es posible utilizar la estructura de control ~if~ de la siguiente
manera, teniendo *mucho cuidado* con los espacios y síntaxis. En este lenguaje
un ~if~ se cierra con  ~fi~.

Si se quiere verificar si un archivo existe o no, si existe entregar el tamaño:

#+NAME: uso_if
#+BEGIN_SRC shell :results replace output  :session condicional :exports both
    archivo="ejercicios/nombres.csv"
    if [ -r "$archivo" ];
    then
    size=$(stat --printf="%s" $archivo)
    modificacion=$(stat --printf="%y" $archivo)
    echo "El archivo "$archivo" existe y tiene un tamaño de "$size" bytes"
    echo "Además fue modificado por última vez en "$modificacion
    fi
#+END_SRC

#+RESULTS: uso_if
: 
: > > > > > > El archivo ejercicios/nombres.csv existe y tiene un tamaño de 192 bytes
: Además fue modificado por última vez en 2018-05-06 14:11:54.346959000 -0300

Se usa la acción *-r archivo* que entrega al sistema la información si *existe y
es legible*. 

Realizar el ejercicio para los archivos *nombres.csv* y *parentesco.csv*.

** ¿Cómo usar condicionales sobre archivos y directorios?

Veamos la siguiente tabla. Los elementos *Operadores* sirven para hacer
revisiones específicas sobre cierta información o archivos.

[[file:./img/tabla_cond_bash.png]]

** Esctructura ~if~, ~else~

Ahora bien, para hacer más completo nuestro programa que revisa la existencia de
un archivo y extrae la *metadata*, lo haremos más elegante y avisaremos que no
existe.

Copiamos el mismo *script* a *segundo_if.sh*. Y añadimos

#+BEGIN_EXAMPLE
archivo="nombres.csv"
if [ -r "$archivo" ]; then
    ...
else
    echo "El archivo"$archivo" no existe"
fi
#+END_EXAMPLE


Realizar el ejercicio para los archivos *nombres.csv*, *parentesco.csv* y *data.csv*

** Estructura ~if~, ~elif~, ~else~

Luego, necesitamos verificar que exista el *archivo* pese a no ser *legible*, por
lo que cambiaremos los permisos de lectura al mismo archivo en caso de ser
necesario.

Copiamos el *script* anterior a *tercer_if.sh* y añadimos.

#+BEGIN_EXAMPLE
archivo="nombres.csv"
if [ -r "$archivo" ]; then
    ...
elif [ -e "$archivo" ]; then
    sudo chmod +r $archivo
else
    echo "El archivo"$archivo" no existe"
fi
#+END_EXAMPLE

Realizar el ejercicio para los archivos *ultimo.csv*, *nombres.csv*, *parentesco.csv* y *data.csv*

** Ejercicio, solicitar un valor, seudo-código.

Primero, una buena práctica para ir aprendiendo, es escribir en seudo-código,
que es describir paso a paso de manera textual las acción es que realiza.

#+BEGIN_EXAMPLE
Solicitar un texto al usuario
La variable asociada es A
Si A es igual a 1=>realiza I
Si no => realiza II
Acciones:
I) Avisa que ingresó 1
II) Avisa que ingresó otro valor
#+END_EXAMPLE

¿Cómo sería esto en un código ~bash~?


** Implementación en ~bash~

Luego de una revisión de lo que hemos aprendido tenemos lo siguiente:
   
#+NAME: ifthen
#+BEGIN_SRC bash
#!/bin/bash

echo 'Ingresa un 1 u otra cosa'
read A

if [ $A == 1 ]; then
  printf 'Ingresaste un uno <$A>'
else
  printf 'Ingresaste otra cosa <$A>'
fi
#+END_SRC


** Reglas básicas para el ~if~ en ~bash~

1. Dejar espacios entre los brackets y la sentencia lógica: ~if [ <condición> ]; then <código>~. 
   
2. Siempre terminar la línea antes de usar alguna palabra reservada. Las palabras
 /if/, /then/, /else/, /elif/ y /fi/ son propias de ~bash~, siempre que usemos
 algunas debemos asegurarnos de terminar la línea. Ya sea con un *;* o con
 un salto de línea.

3. Es una buena práctica usar comillas dentro de las condiciones para evitar problemas con espacios o saltos de línea: ~if [ "$myVar" == "tux" ]~.

** Condiciones sobre archivos o directorios I

A la hora de evaluar condiciones, podemos estar trabajando con diferentes
tipos de datos. Veremos a continuación una lista con algunas de las posibilidades
que permite el lenguaje.

1) -b filename - Block special file
2) -c filename - Special character file
3) -d directoryname - Check for directory Existence
4) -e filename - Check for file existence, regardless of type (node, directory, socket, etc.)
5) -f filename - Check for regular file existence not a directory
6) -G filename - Check if file exists and is owned by effective group ID
7) -G filename set-group-id - True if file exists and is set-group-id
8) -k filename - Sticky bit
9) -L filename - Symbolic link

** Condiciones sobre archivos o directorios II


Otras condiciones posibles sobre archivos o directorios:
   
1) -O filename - True if file exists and is owned by the effective user id
2) -S filename - Check if file is socket
3) -s filename - Check if file is nonzero size
4) -u filename - Check if file set-user-id bit is set
5) -r filename - Check if file is a readable
6) -w filename - Check if file is writable
7) -x filename - Check if file is executable

** Aplicacioń sencilla para archivos

Dado un archivo específico, revisa si existe o no con ese nombre.
   
#+BEGIN_SRC bash
#!/bin/bash

if [ -e guia.org ]; then
    echo "La guía existe";
else
    echo "La guia no existe";
fi
#+END_SRC

** Condiciones para strings

El lenguaje ~bash~ también soporta la creación de comparaciones entre cadentas
de texto.
   
#+NAME:   tab:basic-data
|---------------+-----------------------------------------------------------------------------|
| Condición     | Significado                                                                 |
|---------------+-----------------------------------------------------------------------------|
| STR1 == STR2  | Verdadero si STR1 es igual a STR2                                           |
| STR1 != STR2  | Verdadero si STR1 no es igual a STR2                                        |
| STR1 > STR2   | Verdadero si STR1 es lexicográficamente  |
|               | mayor que STR2, analogo para menor.                                                                            |
| -n STRNOVACIO | Verdadero STRNOVACIO tiene tamaño mayor a 0                                 |
| -z STRVACIO   | Verdadero si STRVACIO está vacío                                            |
| STR =~ REGEX  | STR calza con la expresión regular REGEX                                    |
|---------------+-----------------------------------------------------------------------------|

** Aplicación, verificar que un nombre esté bien escrito.

Se solicita un nombre y luego, definiendo una *regex* se revisa si está bien escrito.
   
#+NAME: exam-str
#+BEGIN_SRC bash
#!/bin/bash
read -p "Ingresa tu nombre " nombre
regex="^[[:upper:]][[:lower:]]+$"
match=$(echo $nombre|grep -P $regex)
if [ -n "$match" ]
then
    echo "Tu nombre está bien escrito "${nombre}
else
    echo "Tu nombre, "${nombre}", está mal escrito"
fi
#+END_SRC
   
=BASH= tiene varias sintaxis para escribir las condiciones, para quien le interese
manejar mejor este lenguaje, puede revisarlas en detalle [[https://linuxacademy.com/blog/linux/conditions-in-bash-scripting-if-statements/][aquí]].

** Condiciones aritméticas

Para comparar valores numéricos (enteros) se usan los comparadores sintácticos.

- eq :: equivalencia
- ne :: no equivalente
- gt :: mayor qué (great than)
- ge :: mayor o igual a  (great or equal to)
- lt :: menor qué (less than)
- le :: menor o igual a (less or equal to)

De esto, para hacer la comparación se construye la tabla:
   
#+NAME:   tab:basic-data
|---------------+---------------------------------------------|
| Condición     | Significado                                 |
|---------------+---------------------------------------------|
| NUM1 -eq NUM2 | Verdadero si los números son iguales        |
| NUM1 -ne NUM2 | Verdadero si NUM1 no es igual a NUM2        |
| NUM1 -gt NUM2 | Verdadero si NUM1 es mayor que NUM2         |
| NUM1 -ge NUM2 | Verdadero si NUM1 es mayor o igual que NUM2 |
| NUM1 -lt NUM2 | Verdadero si NUM1 es menor estricto a NUM2  |
| NUM1 -le NUM2 | Veraddero si NUM1 es menor o igual a NUM2   |
|---------------+---------------------------------------------|

** Aplicación de condiciones aritméticas

En bash estas comparaciones deberían ser, de preferencia, sobre números enteros.
Como cantidades de elementos o similares.

Consultamos la ciudad y la temperatura, comparamos la temperatura y decimos algo
al respecto. Crear un *script* de bash que contenga lo siguiente.

#+BEGIN_SRC bash
read -p "Dime la ciudad: " ciudad
read -p "Dime la temperatura: " temperatura
#+END_SRC 

** Un conjunto de intervalos numéricos en un ~if~

Recordando como se puede visualizar un conjunto de intervalos en un eje de valores numéricos.

[[file:./img/intervalos.png]]


Construimos un programa que nos dice *sensiblemente* o *subjetivamente* que tal
va la temperatura.


** Comparamos la temperatura y actuamos sobre la ciudad

Añadimos el código que entra a ~if~.

#+BEGIN_SRC bash
if [ "$temperatura" -gt "15" ] && \
                   [ "$temperatura" -le "30" ]; then
        echo "Está calido en la ciudad de "$ciudad
elif [ "$temperatura" -gt "30" ]; then
        CIUDAD=$(echo $ciudad|sed 's/\w/\U&/g')
        echo "Está CALUROSO en la ciudad de "$CIUDAD
elif [ "$temperatura" -le "15" ] && \
                   [ "$temperatura" -gt "0" ]; then
        echo "Está fresco en la ciudad de "$ciudad
else
        ciudad_fria=$(echo $ciudad|sed 's/\w/\L&/g')
        echo "Está MUY FRIO en la ciudad de "$ciudad_fria
fi
#+END_SRC


* Control de flujo con ~case~

Puede suceder que se tengan diversos casos para una situación. En vez de
construirlos con una serie de *if* una forma más semántica es revisar por caso,
para eso se usa la estructura *case*.
  
** Uso de ~case~ para análisis de caso

Cuando tenemos un *valor* que puede estar sujeto a distintos casos. Y
necesitamos operar en función a cada caso. Podemos utilizar ~case~, que nos
permite situarnos en cada uno y operar un cierto código.

En seudo-código tenemos lo siguiente:

#+BEGIN_EXAMPLE
case NOMBRE:
^[Aa])
imprimir "Tu nombre empieza con A"
^[Zz])
imprimir "Tu nombre empieza con Z"
por defecto)
imprimir "Tu nombre no empieza con A ni con Z"
#+END_EXAMPLE
 
- url :: https://www.thegeekstuff.com/2010/07/bash-case-statement/

** Aplicación de ~case~

#+BEGIN_SRC bash
read -p "Dame tu nombre: " nombre

case "${nombre:0:1}" in

        [Aa] )
        echo "Tu nombre comienza con A"
        ;;
        [Zz] )
        echo "Tu nombre comienza con Z"
        ;;
        *)
        echo "Tu nombre no comienza con A ni con Z"
esac
#+END_SRC

* Funciones 

** El concepto de función

Una función es la relación entre dos conjuntos. Un conjunto de partida o
*dominio* y un conjunto de salida o *recorrido*. Pueden ser matemáticas o
también de otros tipos.

#+ATTR_LATEX: :width 0.5\textwidth
#+ATTR_HTML: :width 50% :height 50%                                                                                                                         
[[file:./img/funcion.png]]

** Entradas y salidas

Sin embargo, no toda función consiste en una operación matemática. Puede ser la
transformación de cualquier elemento o información en otro.
#+ATTR_HTML: :width 50% :height 50%                                                                                                                         
file:./img/in_out.png


** El uso de las funciones

La creación y uso de funciones nos serán útiles para ahorrar texto de código (no
repetirlo) y hacer que el mismo sea más legible para su lectura y estudio.


#+ATTR_LATEX: :width 0.8\textwidth
#+ATTR_HTML: :width 80% :height 80%                                                                                                                         
[[file:./img/funcion_bash.png]]


** ¿Cómo crear una función en ~bash~?

En ~bash~ el modo de ingreso de los parámetros es textual, ingresa una línea de
texto y separados por *espacio* cada elemento toma una posición.

A diferencia de ~awk~, al definir la *funcion* no se mencionan los argumentos de
entrada o input entre los *()*, sino que se toman de la línea.

#+NAME: param
#+BEGIN_SRC bash
#!/bin/bash
function parametros(){
  echo "nombre de función parámetro" $0
  echo "primer parámetro " $1
  echo "la cantidad total de parámetros es "$#
  echo "todos los parámetros posicionales son "$*
  echo "también sirve con @ "$@
  echo "Usando comillas doble \"$*\""
};
#+END_SRC

** Una función que entregue una palabra al revés

Una función, aplicando lo que ya conocemos, que retorne el string de entrada a
la inversa.

#+BEGIN_SRC bash
function reverse(){
reversa=$(echo "$@"|awk '{
           for(i=length($0);i>=1;i--){
              printf substr($0,i,1)
              }
           }')
echo $reversa
}
#+END_SRC

Se usa el símbolo *$@* para denotar que se utilizarán todas las entradas
entregadas a la función. Es decir, toda la línea, como si fuera el *$0* de awk.

** Activar las funciones disponibles en el archivo.

Se habilitan las funciones para el uso en el ambiente ~bash~ leyendo la fuente
con el comando ~source~. Siendo el archivo *funciones_bash.sh* el que contiene
las funciones.

#+BEGIN_SRC bash
source funciones_bash.sh
#+END_SRC

Luego, probar las funciones disponibles desde la misma terminal.

** Revisar si existe un archivo y si existe entregar la metadata

Ahora, utilizaremos el código que ya escribimos pero lo dejaremos dentro de una
función.

Entonces:

#+BEGIN_SRC bash
function revisa_archivo(){
  archivo="$*"
  if [ -r "$archivo" ]; then
    size=$(stat --printf="%s" $archivo)
    creacion=$(stat --printf="%y" $archivo)
    echo "El archivo "$archivo" existe y tiene \
         un tamaño de "$size" bytes"
    echo "Además fue modificado por última vez \
         en "$creacion
  fi
}
#+END_SRC

Cargar el archivo con ~source~ y probar que pasa para distintos archivos
