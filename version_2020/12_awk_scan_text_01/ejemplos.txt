awk 'BEGIN{print "hola mundo"}{var = $0; print var; print var}END{print "chao mundo"}' << E
echo "hola mundo" | awk '{print length($1)}'
echo "hola mundo" | awk '{print index($0,"mun")}'
echo "hola mundo,Mundo mundo" | awk '{print gensub("[mM]undo","world","g")}'
echo "hola mundo,Mundo mundo" | awk -F ',' '{print gensub("[mM]undo","world","2",$2)}'
echo "hola mundo,Mundo mundo" | awk '{split($0,array,","); print array[2]}'
cat /etc/passwd | awk -F ':' 'BEGIN{printf "%-17s %6s %10s\n","Nombre","ID", "IDf"}{printf "%-17s %6d %10.2f\n",$1,$3,$3}'
